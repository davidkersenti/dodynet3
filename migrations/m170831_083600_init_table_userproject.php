<?php

use yii\db\Migration;

class m170831_083600_init_table_userproject extends Migration
{
     public function up()
    {

        $this->createTable(
            'userproject',
            [
                'userid' => 'integer',
                'projectid' => 'integer',
                'PRIMARY KEY(userid, projectid)',
             
            ],
            'ENGINE=InnoDB'
        );

         $this->addForeignKey(
            'fk-userproject-userid',// This is the fk => the table where i want the fk will be
            'userproject',// son table
            'userid', // son pk	
            'user', // father table
            'id', // father pk
            'CASCADE'
        );	

       $this->addForeignKey(
            'fk-userproject-projectid',// This is the fk => the table where i want the fk will be
            'userproject',// son table
            'projectid', // son pk	
            'project', // father table
            'projectId', // father pk
            'CASCADE'
        );


    }

    public function down()
    {
         $this->dropTable('userproject');

        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_083600_init_table_userproject cannot be reverted.\n";

        return false;
    }
    */
}
