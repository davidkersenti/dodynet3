<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper; 
use app\models\Role;


/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $username
 * @property string $password
 * @property integer $role
 * @property string $auth_Key
 * @property integer $phoneNumber
 * @property integer $age
 * @property string $email
 * @property string $address
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Role $role0
 */
class User extends ActiveRecord implements \yii\web\IdentityInterface
{

    public $roles;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role', 'phoneNumber', 'age', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
                 [['firstname', 'lastname','username', 'password', 'phoneNumber','age'], 'required'],
                 [['roles'],'safe'],
            ['age', 'compare', 'compareValue' => 18, 'operator' => '>='],
			['age', 'compare', 'compareValue' => 60, 'operator' => '<='],
            ['phoneNumber', 'match', 'pattern' => "/^.{10}$/", 'message' => 'The phone number most to be exactly 10 digits'],
            [['address'], 'match', 'pattern' => '/^[a-zA-Zא-ת0-9 -]+$/', 'message' => 'Can not add special characters in this field, please enter only letters and numbers'],
            ['password', 'match', 'pattern' => "/^.{3,16}$/", 'message' => 'the password must be at least 3 characters and max 16 characters '],
            [['firstname', 'lastname', 'username', 'password', 'auth_Key', 'email', 'address'], 'string', 'max' => 255],
            [['role'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role' => 'roleId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'username' => 'Username',
            'password' => 'Password',
            'role' => 'Role',
            'roles' => 'Permission',
            'auth_Key' => 'Auth  Key',
            'phoneNumber' => 'Phone Number',
            'age' => 'Age',
            'email' => 'Email',
            'address' => 'Address',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole0()
    {
        return $this->hasOne(Role::className(), ['roleId' => 'role']);
    }
     public static function findIdentity($id)
    {
        $user = self::findOne($id);
		return $user;
		//return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('Not supported');

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_Key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($auth_Key)
    {
        return $this->auth_Key === $auth_Key;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    
	/** password check default yii
	public function validatePassword($password)
    {
        return $this->password === $password;
    }
	*/
	
	
	//test hashed password

    public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password);
    }
	
	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
	
	//hash password before saving
    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->auth_Key = Yii::$app->security->generateRandomString(32);

        return $return;
    }
	
	//create fullname pseuodo field
	public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }

    public function getUserole() // function for display the permission level at user/view
    {
		$roleArray = Yii::$app->authManager->
					getRolesByUser($this->id);
		$role = array_keys($roleArray)[0];				
		return	$role;
    }
    
   
	
	//A method to get an array of all users models/User
	public static function getUsers()
	{
		$users = ArrayHelper::
					map(self::find()->all(), 'id', 'name');
		return $users;						
	}
		public static function getRoles()
	{

		$rolesObjects = Yii::$app->authManager->getRoles();
		$roles = [];
		foreach($rolesObjects as $id =>$rolObj){
			$roles[$id] = $rolObj->name; 
		}
		
		return $roles; 		
	}


    public function afterSave($insert,$changedAttributes) 
    {
        $return = parent::afterSave($insert, $changedAttributes);

		$auth = Yii::$app->authManager;
		$roleName = $this->roles;   // The role that the user choose  
		$role = $auth->getRole($roleName); // The roles from auth_item table. came from the controller
		if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
			$auth->assign($role, $this->id);
		} 
        else {
			$db = \Yii::$app->db;
			$db->createCommand()->delete('auth_assignment',
				['user_id' => $this->id])->execute();
			$auth->assign($role, $this->id);
		}

        return $return;	
	}
	

	// public function afterSave($insert,$changedAttributes)
    // {
    //     $return = parent::afterSave($insert, $changedAttributes);
		
	// 	$auth = Yii::$app->authManager;
		
	// 	if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
	// 			// auto assign
	// 	}else{
	// 			if(isset($this->role)){
	// 				$role_name = $this->role;
	// 				$role = $auth->getRole($role_name);
	// 				$db = \Yii::$app->db;
	// 				$db->createCommand()->delete('auth_assignment',
	// 					['user_id' => $this->id])->execute();
	// 				$auth->assign($role, $this->id);	
	// 			}	
	// 	}	

    //     return $return;
    // }
    public static function getUsersss()

	{
		$allUsers = self::find()->all();
		$allUsersArray = ArrayHelper::
					map($allUsers, 'id', 'username');

		return $allUsersArray;						
	}
   

    
}