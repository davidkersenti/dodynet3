<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Project;
use app\models\User;

/**
 * ProjectSearch represents the model behind the search form about `app\models\Project`.
 */
class ProjectSearch extends Project
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['projectId', 'teamleader', 'urgency', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['projectName', 'users', 'startDate', 'planeDate', 'endDate', 'location', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Project::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
       

            


       



        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $this->status == -1 ? $this->status = null : $this->status;
        $this->urgency == -1 ? $this->urgency = null : $this->urgency;
       

        // grid filtering conditions
        $query->andFilterWhere([
            'projectId' => $this->projectId,
            'teamleader' => $this->teamleader,
            'startDate' => $this->startDate,
            'planeDate' => $this->planeDate,
            'endDate' => $this->endDate,
            'urgency' => $this->urgency,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'projectName', $this->projectName])
            ->andFilterWhere(['like', 'users', $this->users])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
