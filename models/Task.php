<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;
use app\models\Usertask;
use app\models\User;
use app\models\Project;
use app\models\Status;
use app\models\Level;

/**
 * This is the model class for table "task".
 *
 * @property integer $taskId
 * @property string $taskName
 * @property integer $project
 * @property string $startDate
 * @property string $planeDate
 * @property string $endDate
 * @property integer $level
 * @property integer $status
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Level $level0
 * @property Project $project0
 * @property Status $status0
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project', 'level', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['startDate', 'planeDate', 'endDate'], 'safe'],
            [['description'], 'string'],
            [['taskName'], 'string', 'max' => 255],
            [['level'], 'exist', 'skipOnError' => true, 'targetClass' => Level::className(), 'targetAttribute' => ['level' => 'levelId']],
            [['project'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project' => 'projectId']],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status' => 'statusId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'taskId' => 'Task ID',
            'taskName' => 'Task Name',
            'project' => 'Project',
            'users' => 'Users',
            'startDate' => 'Start Date',
            'planeDate' => 'Schedule',
            'endDate' => 'End Date',
            'level' => 'Level',
            'status' => 'Status',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel0()
    {
        return $this->hasOne(Level::className(), ['levelId' => 'level']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject0()
    {
        return $this->hasOne(Project::className(), ['projectId' => 'project']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(Status::className(), ['statusId' => 'status']);
    }
     ///select2 model//////////////

    public static function getTaskUsersNames()
    {
         $pupil =  ArrayHelper::map(User::find()->all(),'id', 'username');
        return $pupil;
    }

     //////////////// function for converting multi choise array to String in Activity/index.php
    public function getUsersnames2(){
        $str = $this->users;
        $arr = explode(",", $str);
        $arrLength = count($arr);
        for($i=0;$i<=$arrLength-1; $i++){
            $id = intval($arr[$i]);
            $name = \app\models\User::findOne($id)->username; // This function in User.php
            $arr[$i] = $name;
        }
        $names = implode(", ",$arr); //  ', ' will be between the names
       return $names; 

    }

        /////////////////// before save function for converting array names to String
    public function beforeSave($insert)
    {
        
       
        if (parent::beforeSave($insert)) {
            $pupilArray = $_POST['Task']['users'];
            $pupilString = implode(",",array_values($pupilArray));
           $_POST['Task']['users']= $pupilString;
           $this->users = $pupilString;
          
            return true;
        } else {
          
            return false;
        }
    }


    
}