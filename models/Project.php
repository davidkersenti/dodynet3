<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;
use app\models\User;
use app\models\Task;
use app\models\Status;


/**
 * This is the model class for table "project".
 *
 * @property integer $projectId
 * @property string $projectName
 * @property integer $teamleader
 * @property string $startDate
 * @property string $planeDate
 * @property string $endDate
 * @property integer $urgency
 * @property integer $status
 * @property string $location
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Status $status0
 * @property User $teamleader0
 * @property Urgency $urgency0
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teamleader', 'urgency', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['startDate', 'planeDate', 'endDate'], 'safe'],
            [['description'], 'string'],
            [['projectName', 'location'], 'string', 'max' => 255],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status' => 'statusId']],
            [['teamleader'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['teamleader' => 'id']],
            [['urgency'], 'exist', 'skipOnError' => true, 'targetClass' => Urgency::className(), 'targetAttribute' => ['urgency' => 'urgencyId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'projectId' => 'Project ID',
            'projectName' => 'Project Name',
            'teamleader' => 'Teamleader',
            'users' => 'Users',
            'startDate' => 'Start Date',
            'planeDate' => 'Schedule',
            'endDate' => 'End Date',
            'urgency' => 'Urgency',
            'status' => 'Status',
            'location' => 'Location',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(Status::className(), ['statusId' => 'status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamleader0()
    {
        return $this->hasOne(User::className(), ['id' => 'teamleader']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUrgency0()
    {
        return $this->hasOne(Urgency::className(), ['urgencyId' => 'urgency']);
    }
    	public static function getProjects()
	{
		$allProjects = self::find()->all();
		$allProjectsArray = ArrayHelper::
					map($allProjects, 'projectId', 'projectName');
		return $allProjectsArray;						
	}
    ///////////select2 model finish///////////


   //////////////// function for converting multi choise array to String in Activity/index.php
    public function getUsersnames2(){
        $str = $this->users;
        $arr = explode(",", $str);
        $arrLength = count($arr);
        for($i=0;$i<=$arrLength-1; $i++){
            $id = intval($arr[$i]);
            $name = \app\models\User::findOne($id)->username; // This function in User.php
            $arr[$i] = $name;
        }
        $names = implode(", ",$arr); //  ', ' will be between the names
       return $names; 

    }

      /////////////////// before save function for converting array names to String
    public function beforeSave($insert)
    {
        
       
        if (parent::beforeSave($insert)) {
            $pupilArray = $_POST['Project']['users'];
            $pupilString = implode(",",array_values($pupilArray));
           $_POST['Project']['users']= $pupilString;
           $this->users = $pupilString;
          
            return true;
        } else {
          
            return false;
        }
    }
     public static function getProjectsWithAllProjects()
	{
		$allProjects = self::getProjects();
		$allProjects[-1] = 'All Projects';
		$allProjects = array_reverse ( $allProjects, true );
		return $allProjects;	
	}

  


}