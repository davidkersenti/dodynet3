<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "status".
 *
 * @property integer $statusId
 * @property string $statusName
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statusName'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'statusId' => 'Status ID',
            'statusName' => 'Status Name',
        ];
    }
    public static function getStatus()

	{
		$allStatus = self::find()->all();
		$allStatusArray = ArrayHelper::
					map($allStatus, 'statusId', 'statusName');
		return $allStatusArray;						
	}
   public static function getStatusesWithAllStatuses()
	{
		$allStatuses = self::getStatus();
		$allStatuses[-1] = 'All Statuses';
		$allStatuses = array_reverse ( $allStatuses, true );

		return $allStatuses;	
	}
  




 
  
}