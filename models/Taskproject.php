<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "taskproject".
 *
 * @property integer $taskid
 * @property integer $projectid
 *
 * @property Project $project
 * @property Task $task
 */
class Taskproject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taskproject';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['taskid', 'projectid'], 'required'],
            [['taskid', 'projectid'], 'integer'],
            [['projectid'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['projectid' => 'projectId']],
            [['taskid'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['taskid' => 'taskId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'taskid' => 'Taskid',
            'projectid' => 'Projectid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['projectId' => 'projectid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['taskId' => 'taskid']);
    }
}
