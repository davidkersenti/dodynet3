<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "userproject".
 *
 * @property integer $userid
 * @property integer $projectid
 *
 * @property Project $project
 * @property User $user
 */
class Userproject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userproject';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'projectid'], 'required'],
            [['userid', 'projectid'], 'integer'],
            [['projectid'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['projectid' => 'projectId']],
            [['userid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userid' => 'Userid',
            'projectid' => 'Projectid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['projectId' => 'projectid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
}
