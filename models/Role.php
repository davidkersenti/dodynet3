<?php

namespace app\models;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property integer $roleId
 * @property string $roleName
 *
 * @property User[] $users
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['roleName'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'roleId' => 'Role ID',
            'roleName' => 'Role Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['role' => 'roleId']);
    }
     public static function getRolesss()
	{
		$allRoles = self::find()->all();
		$allRolesArray = ArrayHelper::
					map($allRoles, 'roleId', 'roleName');
		return $allRolesArray;						
	}
    public static function getRolesWithAllRoles()
	{
		$allRoles = self::getRolesss();
		$allRoles[-1] = 'All Roles';
		$allRoles = array_reverse ( $allRoles, true );
		return $allRoles;	
	}	
}
