<?php

namespace app\controllers;

use Yii;
use app\models\userproject;
use app\models\UserprojectSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserprojectController implements the CRUD actions for userproject model.
 */
class UserprojectController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all userproject models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserprojectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single userproject model.
     * @param integer $userid
     * @param integer $projectid
     * @return mixed
     */
    public function actionView($userid, $projectid)
    {
        return $this->render('view', [
            'model' => $this->findModel($userid, $projectid),
        ]);
    }

    /**
     * Creates a new userproject model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new userproject();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'userid' => $model->userid, 'projectid' => $model->projectid]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing userproject model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $userid
     * @param integer $projectid
     * @return mixed
     */
    public function actionUpdate($userid, $projectid)
    {
        $model = $this->findModel($userid, $projectid);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'userid' => $model->userid, 'projectid' => $model->projectid]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing userproject model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $userid
     * @param integer $projectid
     * @return mixed
     */
    public function actionDelete($userid, $projectid)
    {
        $this->findModel($userid, $projectid)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the userproject model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $userid
     * @param integer $projectid
     * @return userproject the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($userid, $projectid)
    {
        if (($model = userproject::findOne(['userid' => $userid, 'projectid' => $projectid])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
