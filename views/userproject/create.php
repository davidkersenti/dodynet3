<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\userproject */

$this->title = 'Create Userproject';
$this->params['breadcrumbs'][] = ['label' => 'Userprojects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userproject-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
