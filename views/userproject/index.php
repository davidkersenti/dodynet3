<?php
use yii\helpers\html;

?>

<h1>Project Managment System</h1>

<img src="/project/image/projectlogo.jpg" class="img-rounded" height="200" width="200" style="float:right;">


<p>This system is developed to help the businesses to manage their projects in the best effective way, to ensure that the businesses always will do the projects at defined times, and the most important goal is that the customers will be satisfied with the system. This system is very easy to use, and characterized by high level of usability.</p>


<p>The main roles in this system are: admin, ceo, project manager and task perform.</p>
<p>The three main entities in this system are: users, projects and tasks.</p>
<p><strong>Users</strong> to complete the project, you need users who will perform the tasks and the projects. Any user in the system getting a role and permission (admin, ceo, project manager, task perform).</p>
<p><strong>Projects:</strong>When the user create a project he needs to set: project name, the team leader of the project, assign users, start date, end date, schedule, the priority level (low, medium, high), location and description of the project. </p>
<p><strong>Tasks:</strong> After creating the project, the user needs to create tasks that will help him to complete the projects. When the user creates a task he needs to set: task name, choose the project, assign users, start date, end date, schedule, difficulty level (easy, medium, hard), status (not started yet, in process, done) and describe the task.</p>


 <p><a class="btn btn-success" href="https://ufile.io/b8aez">
Download the full guide</a></p>


