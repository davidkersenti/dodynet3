<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\userproject */

$this->title = 'Update Userproject: ' . $model->userid;
$this->params['breadcrumbs'][] = ['label' => 'Userprojects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->userid, 'url' => ['view', 'userid' => $model->userid, 'projectid' => $model->projectid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="userproject-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
