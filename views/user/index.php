<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/project/image/user.jpg" class="img-rounded" height="120" width="360" style="float: right;">
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <!--<?php echo $this->render('_search', ['model' => $searchModel]); ?>-->

    <p>
      
       <!--<?= Html::button('Create User',  ['value' =>Url::to('user/create'), 'class' => 'btn btn-success','id'=>'modalButton']) ?>-->
    <?php if (\Yii::$app->user->can('createUser')) { ?>
    <p>
        <?= Html::button('Create User',  ['value' =>Url::to('user/create'), 'class' => 'btn btn-success','id'=>'modalButton']) ?>
    </p>
<?php } ?>
    
    </p>
    <?php
    Modal::begin([
'header'=>'<h4>User</h4>',
'id' => 'modal',
'size' => 'modal-lg'
    ]);

    echo "<div id ='modalContent'></div>";
    Modal::end();

    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
       [   
                    'attribute'=>'username',
                    'format'=>'raw',
                    'value' => function($data)
                    {
                        return
                        Html::a($data->username, ['user/view','id'=>$data->id], ['title' => 'View','class'=>'no-pjax']);
                    }
            ],
            // 'id',
            'firstname',
            'lastname',
            'username',
            // 'password',
            // 'role',
            //   [
			// 	'attribute' => 'role',
			// 	'label' => 'Role',
			// 	'format' => 'raw',
			// 	'value' => function($model){
			// 		return $model->role0->roleName;
			// 	},

			// 	'filter'=>Html::dropDownList('UserSearch[role]', $role, $roless, ['class'=>'form-control']),
			// ],	
            // 'auth_Key',
            // 'phoneNumber',
            // 'age',
            // 'email:email',
            // 'address',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
               

            ['class' => 'yii\grid\ActionColumn'
            ],
            
        ],
    ]); ?>
    
</div>