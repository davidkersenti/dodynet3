<?php 

// Working with selector
$tags=array('Satu','Dua','Tiga');
echo CHtml::textField('test','',array('id'=>'test'));
$this->widget('ext.select2.ESelect2',array(
  'selector'=>'#test',
  'options'=>array(
    'tags'=>$tags,
  ),
));
 
<?php $this->widget('ext.select2.ESelect2',array(
  'name'=>'selectInput',
  'data'=>array(
    0=>'Nol',
    1=>'Satu',
    2=>'Dua',
  ),
); ?>
 
// Working with model
<?php $this->widget('ext.select2.ESelect2',array(
  'model'=>$model,
  'attribute'=>'attrName',
  'data'=>array(
    0=>'Nol',
    1=>'Satu',
    2=>'Dua',
  ),
); ?>
 
 
// Optgroup
$data=array(
  'one'=>array(
    '1'=>'Satu',
    '2'=>'Dua',
    '3'=>'Tiga',
  ),
  'two'=>array(
    '4'=>'Sidji',
    '5'=>'Loro',
    '6'=>'Telu',
  ),
  'three'=>array(
    '7'=>'Hiji',
    '8'=>'Dua',
    '9'=>'Tilu',
  ),
);
 
$this->widget('ext.select2.ESelect2',array(
  'name'=>'testing',
  'data'=>$data,
)); 
 
 
$data=array(
  '1'=>'Satu',
  '2'=>'Dua',
  '3'=>'Tiga',
);
 
 
// Multiple data
$this->widget('ext.select2.ESelect2',array(
  'name'=>'ajebajeb',
  'data'=>$data,
  'htmlOptions'=>array(
    'multiple'=>'multiple',
  ),
));
 
// Placeholder
$this->widget('ext.select2.ESelect2',array(
  'name'=>'asik2x',
  'data'=>$data,
  'options'=>array(
    'placeholder'=>'Keren ya?',
    'allowClear'=>true,
  ),
));
?>



