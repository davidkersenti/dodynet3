<?php
use yii\bootsrap\Model;
use yii\helpers\html;

/* @var $this yii\web\View */

$this->title = 'Project Management';
?>
<div class="site-index">

    <div class="jumbotron">
    


        <h2>Welcome To Project Management System!</h2>
        <p class="lead">The Effective Way To Manage Your Projects</p> 
        <img id="contactform-verifycode-image" src="/project/image/home.jpg" alt="school" height="400" width="1000">
        <p><a class="btn btn-link" style="width:200px;" href="?r=site/login">Get Started</a> 


    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">

 
                <h2>Project</h2>
                <p>For your business it is necessary to create projects. In this page you can manage your project: create updates and delete them. To create projects you need to assign users, Team Leader, set schedules, set status and set the priority. Since this will give you best view for managing your projects.
            </p>


                <p><a class="btn btn-primary" href="?r=project/index" style="height:40px;width:110px">View Projects</a></p>

            </div>
            <div class="col-lg-4">
                <h2>Tasks</h2>

                <p>
Tasks must be selected for project execution. In this page you can manage your tasks: create, update and delete. To create tasks you need to assign only users who belong to the project, to set schedules, set status and more.
</p>
                
                
                <p><a class="btn btn-warning" href="?r=task/index" style="height:40px;width:110px">View Tasks</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Users</h2>

                <p>
Each project has to select users and define roles: manager, task performer, user and more. In Addition you will give each user its own authorization in order to keep maintaining classified information.
</p>
 
                <p><a class="btn btn-success" href="?r=user/index" style="height:40px;width:110px">View Users</a></p>
            </div>
        </div>

    </div>
</div>

