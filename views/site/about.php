<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><b>  
	
We are David Kersenti & Oz Yashar. <br>
We are currently graduate students of College of Engineering at Jerusalem in our 4th year. <br>
We have been working together for the project for the Information System course. <br> 
For this project we have been  using a different technical tools like Technology of <br>
YII2 FRAMEWORK, CSS, HTML, PHP & MYSQl &nbsp;    <i class="glyphicon glyphicon-thumbs-up"></i> <br>
&copy; 2017

<p><strong>Contact us:</strong></p>
<p><strong>davidkersenti@gmail.com</strong></p>
<p><strong>ozyasahr.oy@gmail.com</strong></p>
 
		<img id="contactform-verifycode-image" src="/yii/basic/images/team.jpg" alt="" height="202" width="602">

<!---------------------------- google map ----------->
<html>
  <head>
    <style>
       #map {
        height: 600px;
        width: 80%;
       }
    </style>
  </head>
  <body>
  <br>
    <h3><b>Our College </b></h3>
    <div id="map"></div>
    <script>
      function initMap() {
        var uluru = {lat: 31.768876, lng: 35.193629};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 18,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFcgKO2_GP0jNcxqEavLLtmyL5L_Fn0x4&callback=initMap">
    </script>
  </body>
</html>