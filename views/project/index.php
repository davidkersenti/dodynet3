<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;
use yii\widgets\Pjax;
// use kartik\export\ExportMenu;



/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/project/image/project.jpg" class="img-rounded" height="100" width="324" style="float: right;">
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <!--<?php  echo $this->render('_search', ['model' => $searchModel]); ?>-->

     <p>
      
       <!--<?= Html::button('Create Project',  ['value' =>Url::to('project/create'), 'class' => 'btn btn-success','id'=>'modalButton']) ?>-->
    <?php if (\Yii::$app->user->can('createProject')) { ?>
    <p>
        <?= Html::button('Create Project',  ['value' =>Url::to('project/create'), 'class' => 'btn btn-success','id'=>'modalButton']) ?>
    </p>
<?php } ?>
    
    </p>
  

    <!--pop up-->
    <?php
    Modal::begin([
'header'=>'<h4>Project</h4>',
'id' => 'modal',
'size' => 'modal-lg'
    ]);

    echo "<div id ='modalContent'></div>";
    Modal::end();

    ?>
      <?php Pjax::begin();?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' =>function($model){
            if($model->urgency =='1')
            {
                return ['class'=>'success'];
            }
            else if($model->urgency =='2')
            {
                return ['class'=>'warning'];
            }
            else{
                return ['class'=>'danger'];

            }


        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'projectId',
            'projectName',
         'teamleader',
           

            // 'startDate',

             // 'planeDate',
            [
                'attribute' => 'startDate',
                'value' => 'startDate',
                'format' => 'raw',
                'filter' => DatePicker::widget([
    'model' => $searchModel,
    'attribute' => 'startDate',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ]
]),

            ],
            // 'planeDate',
            [
                'attribute' => 'planeDate',
                'value' => 'planeDate',
                'format' => 'raw',
                'filter' => DatePicker::widget([
    'model' => $searchModel,
    'attribute' => 'planeDate',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ]
]),

            ],

            


            // 'endDate',
               [
                'attribute' => 'endDate',
                'value' => 'endDate',
                'format' => 'raw',
                'filter' => DatePicker::widget([
    'model' => $searchModel,
    'attribute' => 'endDate',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ]
]),

            ],
            // 'urgency',
            [
				'attribute' => 'urgency',
				'label' => 'Urgency',
				'format' => 'raw',
				'value' => function($model){
					return $model->urgency0->urgencyName;
				},
				'filter'=>Html::dropDownList('ProjectSearch[urgency]', $urgency, $urgencys, ['class'=>'form-control']),
			],


        //    'status',
            //   [
			// 	'attribute' => 'status',
			// 	'label' => 'Status',
			// 	'format' => 'raw',
			// 	'value' => function($model){
			// 		return $model->status0->statusName;
			// 	},
			// 	'filter'=>Html::dropDownList('ProjectSearch[status]', $status, $statuses, ['class'=>'form-control']),
			// ],

            'location',
             'description:ntext',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
             [
                'attribute' => 'users',
                'label' => 'Users',
                'format' => 'raw',

                'value' => function($model){
                    return $model->Usersnames2; //This function in Activity.php

                },
            ],
             

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

   
</div>