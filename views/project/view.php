<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = $model->projectName;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->projectId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->projectId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'projectId',
            'projectName',
            'teamleader',
            'startDate',
            'planeDate',
            'endDate',
            'urgency',
            // 'status',
            'location',
            'description:ntext',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
              [
                'attribute' => 'users',
                'label' => 'Users',
                'format' => 'raw',
                'value' => function($model){
                    return $model->Usersnames2; //This function in Activity.php

                },
            ],
            
        

            

        ],
       
    ]) ?>
    

</div>