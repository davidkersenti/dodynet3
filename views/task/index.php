<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/project/image/task.jpg" class="img-rounded" height="120" width="324" style="float: right;">
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <!--<?php  echo $this->render('_search', ['model' => $searchModel]); ?>-->

    <p>
      
       <!--<?= Html::button('Create Task',  ['value' =>Url::to('task/create'), 'class' => 'btn btn-success','id'=>'modalButton']) ?>-->
    <?php if (\Yii::$app->user->can('createTask')) { ?>
    <p>
        <?= Html::button('Create Task',  ['value' =>Url::to('task/create'), 'class' => 'btn btn-success','id'=>'modalButton']) ?>
    </p>
<?php } ?>
    
    </p>
    <!--pop up-->
    <?php
    Modal::begin([
'header'=>'<h4>Task</h4>',
'id' => 'modal',
'size' => 'modal-lg'
    ]);

    echo "<div id ='modalContent'></div>";
    Modal::end();

    ?>

    
        
    <?php Pjax::begin();?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($model){
            //if($model->status0->statusName =='Not started yet')
            if($model->status =='1')
            {                                //// 1 is status not started yet
               
             return['class'=>'danger'];

            } else if($model->status =='2')
            {

                return['class'=>'warning'];
            }
            else{
                return['class'=>'success'];

            }

        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
                  
       [   
                    'attribute'=>'taskName',
                    'format'=>'raw',
                    'value' => function($data)
                    {
                        return
                        Html::a($data->taskName, ['task/view','taskId'=>$data->taskId], ['title' => 'View','class'=>'no-pjax']);
                    }
            ],
            
                
    

            // 'taskId',
            // 'taskName',
             [
				'attribute' => 'project',
				'label' => 'Project',
				'format' => 'raw',
				'value' => function($model){
					return $model->project0->projectName;
				},
				'filter'=>Html::dropDownList('TaskSearch[project]', $project, $projects, ['class'=>'form-control']),
			],


            
            // 'startDate',
             [
                'attribute' => 'startDate',
                'value' => 'startDate',
                'format' => 'raw',
                'filter' => DatePicker::widget([
    'model' => $searchModel,
    'attribute' => 'startDate',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ]
]),

            ],
            // 'planeDate',
               // 'planeDate',
            [
                'attribute' => 'planeDate',
                'value' => 'planeDate',
                'format' => 'raw',
                'filter' => DatePicker::widget([
    'model' => $searchModel,
    'attribute' => 'planeDate',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ]
]),

            ],


            // 'endDate',
              [
                'attribute' => 'endDate',
                'value' => 'endDate',
                'format' => 'raw',
                'filter' => DatePicker::widget([
    'model' => $searchModel,
    'attribute' => 'endDate',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ]
]),

            ],

            //  'level',
             [
				'attribute' => 'level',
				'label' => 'Level',
				'format' => 'raw',
				'value' => function($model){
					return $model->level0->levelname;
				},
				'filter'=>Html::dropDownList('TaskSearch[level]', $level, $levels, ['class'=>'form-control']),
			],




                 [
				'attribute' => 'status',
				'label' => 'Status',
				'format' => 'raw',
				'value' => function($model){
					return $model->status0->statusName;
				},
				'filter'=>Html::dropDownList('TaskSearch[status]', $status, $statuses, ['class'=>'form-control']),
			],
             
            'description:ntext',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
              [
                'attribute' => 'users',
                'label' => 'Users',
                'format' => 'raw',

                'value' => function($model){
                    return $model->Usersnames2; //This function in Activity.php

                },
            ],
             


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>