<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;
use app\models\Project;
use kartik\widgets\Select2;
use app\models\Level;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use app\models\Task;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'taskName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'project')->
				dropDownList(Project::getProjects())  ?>

                 <!--  Select2 in form !-->

    <?php $data = Task::getTaskUsersNames(); ?> 
    
    <?= $form->field($model, 'users')->widget(Select2::classname(), [
        'data' => [$data], // $data is declare above , and don't forget before save function.

        'size' => Select2::SMALL,
        'options' => ['placeholder' => 'Select users ...', 'multiple' => true],
        'pluginOptions' => [
        //'tokenSeparators' => [',', ' '], // meybe
        'allowClear' => true //,
        //'tags' => true
            ],
        ]); 
    ?>

    <!--  Select2 finish in form !-->
    

  <?=$form->field($model, 'startDate')->widget(DatePicker::className(), [
 
    'attribute' => 'startDate',
     'options' => ['placeholder' => 'choose start date'],
  
    
    'type' => DatePicker::TYPE_RANGE,
    'attribute2' => 'endDate',
     'options' => ['placeholder' => 'choose start date'],
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
    ]
]);
?>

     <?= $form->field($model, 'planeDate')->widget(DatePicker::className(), [
		//'size' => 'lg',
		'language'=>'en',
        'options' => ['placeholder' => 'choose plane date'],
		'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
            //'todayHighlight' => true // Default today			
        ]
	]);?>


    <!--<?= $form->field($model, 'endDate')->textInput() ?>-->

   <?= $form->field($model, 'level')->
				dropDownList(Level::getLevels())  ?>

    
  <?= $form->field($model, 'status')->

				dropDownList(Status::getStatus())  ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <!--<?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>