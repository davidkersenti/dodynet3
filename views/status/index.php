<!DOCTYPE html>
<html>
<body>
<img src="/project/image/manager.jpg" class="img-rounded" height="250" width="250" style="float: right;">

<article>
  <h1>Project Manager</h1>
  <p>A project manager is a professional in the field of project management. Project managers have the responsibility of the planning, procurement and execution of a project, in any domain of engineering. Project managers are first point of contact for any issues or discrepancies arising from within the heads of various departments in an organization before the problem escalates to higher authorities. Project management is the responsibility of a project manager. This individual seldom participates directly in the activities that produce the end result, but rather strives to maintain the progress, mutual interaction and tasks of various parties in such a way that reduces the risk of overall failure, maximizes benefits, and minimizes costs.</p>
 
 <p><strong>About the role in the system:</strong> In this system the Project Manager have permissions to view, create, edit and delete tasks
In addition, he can to assign user to tasks and assign user to projects.
</p>
 

  
</article>




</body>
</html>